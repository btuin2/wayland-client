This is an implementation of a wayland client in C++, based on Drew Devault's book [wayland-book](https://wayland-book.com/).

It goes a bit further than the book, with the implementation of a scaling factor for example, and fullscreen.

Press f to enter fullscreen, escape to quit fullscreen, and q to close the program.
