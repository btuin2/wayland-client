/*
wayland-client
Copyright (C) 2022 BTuin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pointer.h"

enum pointer_event_mask : int {
	POINTER_EVENT_ENTER = 1 << 0,
	POINTER_EVENT_LEAVE = 1 << 1,
	POINTER_EVENT_MOTION = 1 << 2,
	POINTER_EVENT_BUTTON = 1 << 3,
	POINTER_EVENT_AXIS = 1 << 4,
	POINTER_EVENT_AXIS_SOURCE = 1 << 5,
	POINTER_EVENT_AXIS_STOP = 1 << 6,
	POINTER_EVENT_AXIS_DISCRETE = 1 << 7,
};

void pointer_enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
                   wl_surface *surface, wl_fixed_t surface_x,
                   wl_fixed_t surface_y) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_ENTER;
	state->pointer_event.serial = serial;
	state->pointer_event.surface_x = surface_x;
	state->pointer_event.surface_y = surface_y;
}

void pointer_leave(void *data, wl_pointer *pointer, uint32_t serial,
                   wl_surface *surface) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.serial = serial;
	state->pointer_event.event_mask |= POINTER_EVENT_LEAVE;
}

void pointer_motion(void *data, wl_pointer *pointer, uint32_t time,
                    wl_fixed_t surface_x, wl_fixed_t surface_y) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_MOTION;
	state->pointer_event.time = time;
	state->pointer_event.surface_x = surface_x;
	state->pointer_event.surface_y = surface_y;
}

void pointer_button(void *data, wl_pointer *pointer, uint32_t serial,
                    uint32_t time, uint32_t button, uint32_t key_state) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_BUTTON;
	state->pointer_event.time = time;
	state->pointer_event.serial = serial;
	state->pointer_event.button = button;
	state->pointer_event.state = key_state;
}

void pointer_axis(void *data, wl_pointer *pointer, uint32_t time, uint32_t axis,
                  wl_fixed_t value) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_AXIS;
	state->pointer_event.time = time;
	state->pointer_event.axes[axis].valid = true;
	state->pointer_event.axes[axis].value = value;
}
void pointer_axis_source(void *data, wl_pointer *pointer,
                         uint32_t axis_source) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_AXIS_SOURCE;
	state->pointer_event.axis_source = true;
}

void pointer_axis_stop(void *data, wl_pointer *pointer, uint32_t time,
                       uint32_t axis) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.time = time;
	state->pointer_event.event_mask |= POINTER_EVENT_AXIS_STOP;
	state->pointer_event.axes[axis].valid = true;
}

void pointer_axis_discrete(void *data, wl_pointer *pointer, uint32_t axis,
                           int32_t discrete) {
	client_state *state = static_cast<client_state *>(data);
	state->pointer_event.event_mask |= POINTER_EVENT_AXIS_DISCRETE;
	state->pointer_event.axes[axis].valid = true;
	state->pointer_event.axes[axis].discrete = discrete;
}

void pointer_frame(void *data, struct wl_pointer *wl_pointer) {
	struct client_state *state = static_cast<client_state *>(data);
	struct pointer_event *event = &state->pointer_event;
	std::cerr << "pointer frame : " << event->time << std::endl;

	if (event->event_mask & POINTER_EVENT_ENTER) {
		std::cerr << "entered " << wl_fixed_to_double(event->surface_x) << ", "
		          << wl_fixed_to_double(event->surface_y) << std::endl;
	}

	if (event->event_mask & POINTER_EVENT_LEAVE) {
		std::cerr << "leave" << std::endl;
	}

	if (event->event_mask & POINTER_EVENT_MOTION) {
		std::cerr << "motion " << wl_fixed_to_double(event->surface_x) << ", "
		          << wl_fixed_to_double(event->surface_y) << std::endl;
		state->pointer_x = wl_fixed_to_double(event->surface_x);
		state->pointer_y = wl_fixed_to_double(event->surface_y);
	}

	if (event->event_mask & POINTER_EVENT_BUTTON) {
		std::string state = event->state == WL_POINTER_BUTTON_STATE_RELEASED
		                        ? "released"
		                        : "pressed";
		std::cerr << "button " << event->button << ", " << state.c_str()
		          << std::endl;
	}

	uint32_t axis_events = POINTER_EVENT_AXIS | POINTER_EVENT_AXIS_SOURCE |
	                       POINTER_EVENT_AXIS_STOP |
	                       POINTER_EVENT_AXIS_DISCRETE;

	std::array<std::string, 2> axis_name;
	axis_name[WL_POINTER_AXIS_VERTICAL_SCROLL] = "vertical";
	axis_name[WL_POINTER_AXIS_HORIZONTAL_SCROLL] = "horizontal";

	std::array<std::string, 4> axis_source;
	axis_source[WL_POINTER_AXIS_SOURCE_WHEEL] = "wheel";
	axis_source[WL_POINTER_AXIS_SOURCE_FINGER] = "finger";
	axis_source[WL_POINTER_AXIS_SOURCE_CONTINUOUS] = "continuous";
	axis_source[WL_POINTER_AXIS_SOURCE_WHEEL_TILT] = "wheel tilt";

	if (event->event_mask & axis_events) {
		for (size_t i = 0; i < 2; ++i) {
			if (!event->axes[i].valid) {
				continue;
			}
			std::cerr << axis_name[i] << " axis " << std::endl;
			if (event->event_mask & POINTER_EVENT_AXIS) {
				std::cerr << "value "
				          << wl_fixed_to_double(event->axes[i].value)
				          << std::endl;
			}
			if (event->event_mask & POINTER_EVENT_AXIS_DISCRETE) {
				std::cerr << "discrete " << event->axes[i].discrete
				          << std::endl;
			}
			if (event->event_mask & POINTER_EVENT_AXIS_SOURCE) {
				std::cerr << "via " << axis_source[event->axis_source]
				          << std::endl;
			}
			if (event->event_mask & POINTER_EVENT_AXIS_STOP) {
				std::cerr << "(stopped) " << std::endl;
			}
		}
	}

	memset(event, 0, sizeof(*event));
}

void keyboard_keymap(void *data, wl_keyboard *keyboard, uint32_t format,
                     int32_t fd, uint32_t size) {
	client_state *state = static_cast<client_state *>(data);
	assert(format == WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1);

	char *map_shm{
	    static_cast<char *>(mmap(nullptr, size, PROT_READ, MAP_SHARED, fd, 0))};

	xkb_keymap *keymap = xkb_keymap_new_from_string(
	    state->xkb_context, map_shm, XKB_KEYMAP_FORMAT_TEXT_V1,
	    XKB_KEYMAP_COMPILE_NO_FLAGS);

	munmap(map_shm, size);
	close(fd);

	xkb_state *state_xkb = xkb_state_new(keymap);
	xkb_keymap_unref(state->xkb_keymap);
	xkb_state_unref(state->xkb_state);

	state->xkb_keymap = keymap;
	state->xkb_state = state_xkb;
}

void keyboard_enter(void *data, wl_keyboard *keyboard, uint32_t serial,
                    wl_surface *surface, wl_array *keys) {
	client_state *state = static_cast<client_state *>(data);
	std::cerr << "keyboard enter ; key pressed are :" << std::endl;

	int size_arr = wl_fixed_to_int(keys->size);

	uint32_t *pointer_begin = static_cast<uint32_t *>(keys->data);
	uint32_t *pointer_end = pointer_begin + size_arr;

	for (uint32_t *key = pointer_begin; key != pointer_end; key++) {
		char buf[128];
		xkb_keysym_t sym =
		    xkb_state_key_get_one_sym(state->xkb_state, *key + 8);
		xkb_keysym_get_name(sym, buf, sizeof(buf));
		std::cerr << "sym :" << buf << " (" << sym << ")" << std::endl;

		xkb_state_key_get_utf8(state->xkb_state, *key + 8, buf, sizeof(buf));
		std::cerr << "utf8 : " << buf << std::endl;
	}
}

void keyboard_key(void *data, wl_keyboard *keyboard, uint32_t serial,
                  uint32_t time, uint32_t key, uint32_t key_state) {
	client_state *state = static_cast<client_state *>(data);

	char buf[128];
	uint32_t keycode = key + 8;
	xkb_keysym_t sym = xkb_state_key_get_one_sym(state->xkb_state, keycode);
	xkb_keysym_get_name(sym, buf, sizeof(buf));
	std::string action =
	    key_state == WL_KEYBOARD_KEY_STATE_PRESSED ? "press" : "release";

	std::cerr << "key " << action << " : "
	          << "sym : " << sym << std::endl;
	xkb_state_key_get_utf8(state->xkb_state, keycode, buf, sizeof(buf));
	std::cerr << "utf8 : " << buf << std::endl;

	if (sym == XKB_KEY_f and action == "press") {
		std::cout << "Key \"" << buf << "\" has been pressed" << std::endl;
		xdg_toplevel_set_fullscreen(state->toplevel, state->output);
	} else if (sym == XKB_KEY_Escape and action == "press") {
		std::cout << "Key \"" << buf << "\" has been pressed" << std::endl;
		xdg_toplevel_unset_fullscreen(state->toplevel);
	} else if (sym == XKB_KEY_q and action == "press") {
		exit(0);
	}
}

void keyboard_leave(void *data, wl_keyboard *keyboard, uint32_t serial,
                    wl_surface *surface) {
	std::cerr << "keyboard leave" << std::endl;
}

void keyboard_modifiers(void *data, wl_keyboard *keyboard, uint32_t serial,
                        uint32_t mods_depressed, uint32_t mods_latched,
                        uint32_t mods_locked, uint32_t group) {
	client_state *state = static_cast<client_state *>(data);
	xkb_state_update_mask(state->xkb_state, mods_depressed, mods_latched,
	                      mods_locked, 0, 0, group);
}

void keyboard_repeat_info(void *data, wl_keyboard *keyboard, int32_t rate,
                          int32_t delay) {}

void xdg_toplevel_configure(void *data, xdg_toplevel *toplevel, int32_t width,
                            int32_t heigh, wl_array *states) {
	client_state *state = static_cast<client_state *>(data);
	if (heigh == 0 || width == 0) {
		return;
	}
	state->width = width;
	state->height = heigh;
}

void xdg_toplevl_close(void *data, xdg_toplevel *toplevel) {
	client_state *state = static_cast<client_state *>(data);
	state->closed = true;
}
