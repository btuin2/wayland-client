/*
wayland-client
Copyright (C) 2022 BTuin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POINTER_H_
#define POINTER_H_

#include "xdg-shell-client-protocol.h"
#include <array>
#include <cassert>
#include <cstring>
#include <iostream>
#include <memory>
#include <span>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client.h>
#include <xkbcommon/xkbcommon.h>

enum pointer_event_mask : int;

struct pointer_event {
	uint32_t event_mask;
	wl_fixed_t surface_x, surface_y;
	uint32_t button, state;
	uint32_t time;
	uint32_t serial;
	struct {
		bool valid;
		wl_fixed_t value;
		int32_t discrete;
	} axes[2];
	uint32_t axis_source;
};

struct client_state {
	wl_display *display;
	wl_output *output;
	wl_compositor *compositor;
	wl_shm *shm;
	xdg_wm_base *wm_base;
	xdg_surface *surface_xdg;
	wl_surface *surface_wl;
	wl_registry *registry;
	wl_seat *seat;
	xdg_toplevel *toplevel;

	wl_keyboard *keyboard;
	wl_pointer *pointer;
	wl_touch *touch;
	// State
	float offset;
	uint32_t last_frame;
	uint32_t scale;
	int width, height;
	bool closed;
	struct pointer_event pointer_event;

	struct xkb_state *xkb_state;
	struct xkb_context *xkb_context;
	struct xkb_keymap *xkb_keymap;
	double pointer_x;
	double pointer_y;
};

void pointer_enter(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
                   wl_surface *surface, wl_fixed_t surface_x,
                   wl_fixed_t surface_y);

void pointer_leave(void *data, wl_pointer *pointer, uint32_t serial,
                   wl_surface *surface);

void pointer_motion(void *data, wl_pointer *pointer, uint32_t time,
                    wl_fixed_t surface_x, wl_fixed_t surface_y);

void pointer_button(void *data, wl_pointer *pointer, uint32_t serial,
                    uint32_t time, uint32_t button, uint32_t key_state);

void pointer_axis(void *data, wl_pointer *pointer, uint32_t time, uint32_t axis,
                  wl_fixed_t value);

void pointer_axis_source(void *data, wl_pointer *pointer, uint32_t axis_source);

void pointer_axis_stop(void *data, wl_pointer *pointer, uint32_t time,
                       uint32_t axis);

void pointer_axis_discrete(void *data, wl_pointer *pointer, uint32_t axis,
                           int32_t discrete);

void pointer_frame(void *data, struct wl_pointer *wl_pointer);

void keyboard_keymap(void *data, wl_keyboard *keyboard, uint32_t format,
                     int32_t fd, uint32_t size);

void keyboard_enter(void *data, wl_keyboard *keyboard, uint32_t serial,
                    wl_surface *surface, wl_array *keys);

void keyboard_key(void *data, wl_keyboard *keyboard, uint32_t serial,
                  uint32_t time, uint32_t key, uint32_t key_state);

void keyboard_leave(void *data, wl_keyboard *keyboard, uint32_t serial,
                    wl_surface *surface);

void keyboard_modifiers(void *data, wl_keyboard *keyboard, uint32_t serial,
                        uint32_t mods_depressed, uint32_t mods_latched,
                        uint32_t mods_locked, uint32_t group);

void keyboard_repeat_info(void *data, wl_keyboard *keyboard, int32_t rate,
                          int32_t delay);

void xdg_surface_configure(void *data, xdg_surface *surface, uint32_t serial);

void xdg_toplevel_configure(void *data, xdg_toplevel *toplevel, int32_t width,
                            int32_t heigh, wl_array *states);

void xdg_toplevl_close(void *data, xdg_toplevel *toplevel);

const xdg_toplevel_listener toplevel_listener = {
    .configure = xdg_toplevel_configure,
    .close = xdg_toplevl_close,
};

#endif // POINTER_H_
