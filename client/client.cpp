/*
wayland-client
Copyright (C) 2022 BTuin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "client.h"
#include "pointer.h"
#include "shm_file.h"
#include "xdg-shell-client-protocol.h"
#include <cassert>
#include <cstring>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sys/mman.h>
#include <wayland-client.h>

const wl_buffer_listener buffer_listener = {
    .release = buffer_release,
};

wl_pointer_listener pointer_listener = {
    .enter = pointer_enter,
    .leave = pointer_leave,
    .motion = pointer_motion,
    .button = pointer_button,
    .axis = pointer_axis,
    .frame = pointer_frame,
    .axis_source = pointer_axis_source,
    .axis_stop = pointer_axis_stop,
    .axis_discrete = pointer_axis_discrete,
};

const struct wl_keyboard_listener keyboard_listener = {
    .keymap = keyboard_keymap,
    .enter = keyboard_enter,
    .leave = keyboard_leave,
    .key = keyboard_key,
    .modifiers = keyboard_modifiers,
    .repeat_info = keyboard_repeat_info,
};

const struct xdg_surface_listener xdg_surface_listener = {
    .configure = xdg_surface_configure,
};

const wl_seat_listener seat_listener = {
    .capabilities = seat_capabilies,
    .name = seat_name,
};

const struct xdg_wm_base_listener wm_base_listener = {
    .ping = wm_base_ping,
};

struct wl_callback_listener surface_frame_listener = {
    .done = surface_frame_done,
};

const struct wl_registry_listener registry_listener = {
    .global = global_registry_handler,
    .global_remove = global_registry_remover};

const struct wl_output_listener output_listener = {
    .geometry = display_handle_geometry,
    .mode = display_handle_mode,
    .done = display_handle_done,
    .scale = output_scale,
};

void buffer_release(void *data, wl_buffer *buffer) {
	wl_buffer_destroy(buffer);
}

wl_buffer *draw_frame(client_state *state) {
	const int width = state->width * state->scale,
	          height = state->height * state->scale;
	const int stride = width * 4;
	const int size = stride * height;
	wl_surface_set_buffer_scale(state->surface_wl, state->scale);
	int fd = allocate_shm_file(size);

	if (fd == -1) {
		return nullptr;
	}

	uint32_t *data = static_cast<uint32_t *>(
	    mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));

	if (data == MAP_FAILED) {
		close(fd);
		return nullptr;
	}

	wl_shm_pool *pool = wl_shm_create_pool(state->shm, fd, size);
	wl_buffer *buffer = wl_shm_pool_create_buffer(
	    pool, 0, width, height, stride, WL_SHM_FORMAT_XRGB8888);
	wl_shm_pool_destroy(pool);
	close(fd);

	/* Draw checkerboxed background */
	int offset = static_cast<int>(state->offset) % 8;
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (x == static_cast<int>(state->pointer_x * state->scale) or
			    y == static_cast<int>(state->pointer_y * state->scale)) {
				data[y * width + x] = 0xffffff00; // Draw yellow lines
			} else if (((x / state->scale + offset) +
			            (y / state->scale + offset) / 8 * 8) %
			               16 <
			           8)
				data[y * width + x] = 0x00666666;
			else
				data[y * width + x] = 0x00EEEEEE;
		}
	}

	munmap(data, size);
	wl_buffer_add_listener(buffer, &buffer_listener, nullptr);
	return buffer;
}

void xdg_surface_configure(void *data, xdg_surface *surface, uint32_t serial) {
	client_state *state = static_cast<client_state *>(data);
	xdg_surface_ack_configure(surface, serial);

	wl_buffer *buffer = draw_frame(state);
	wl_surface_attach(state->surface_wl, buffer, 0, 0);
	wl_surface_commit(state->surface_wl);
}

void wm_base_ping(void *data, xdg_wm_base *wm_base, uint32_t serial) {
	xdg_wm_base_pong(wm_base, serial);
}

void surface_frame_done(void *data, wl_callback *cb, uint32_t time) {
	wl_callback_destroy(cb);

	client_state *state = static_cast<client_state *>(data);
	cb = wl_surface_frame(state->surface_wl);
	wl_callback_add_listener(cb, &surface_frame_listener, state);

	if (state->last_frame != 0) {
		int elapsed = time - state->last_frame;
		state->offset += elapsed / 1000.0 * 24;
	}

	struct wl_buffer *buffer = draw_frame(state);
	wl_surface_attach(state->surface_wl, buffer, 0, 0);
	wl_surface_damage_buffer(state->surface_wl, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_commit(state->surface_wl);

	state->last_frame = time;
}

void do_nothing(void *data, wl_output *output, uint32_t id) { return; }

void output_scale(void *data, wl_output *output, int32_t factor) {
	std::cout << "Scale factor : " << factor << std::endl;
	client_state *state = static_cast<client_state *>(data);
	state->scale = factor;
}

void display_handle_geometry(void *data, wl_output *output, int x, int y,
                             int physical_width, int physical_height,
                             int subpixel, const char *make, const char *model,
                             int transform) {}

void display_handle_mode(void *data, wl_output *output, uint32_t flags,
                         int width, int height, int refresh) {}

void display_handle_done(void *data, wl_output *output) {}

void seat_capabilies(void *data, wl_seat *seat, uint32_t capabilities) {
	client_state *state = static_cast<client_state *>(data);

	bool have_pointer = capabilities & WL_SEAT_CAPABILITY_POINTER;

	if (have_pointer && state->pointer == nullptr) {
		state->pointer = wl_seat_get_pointer(state->seat);
		wl_pointer_add_listener(state->pointer, &pointer_listener, state);
	} else if (!have_pointer && state->pointer != nullptr) {
		wl_pointer_release(state->pointer);
		state->pointer = nullptr;
	}

	bool have_keyboard = capabilities & WL_SEAT_CAPABILITY_KEYBOARD;

	if (have_keyboard && state->keyboard == nullptr) {
		state->keyboard = wl_seat_get_keyboard(state->seat);
		wl_keyboard_add_listener(state->keyboard, &keyboard_listener, state);
	} else if (!have_keyboard && state->keyboard != nullptr) {
		wl_keyboard_release(state->keyboard);
		state->keyboard = nullptr;
	}
}

void seat_name(void *data, wl_seat *seat, const char *name) {
	std::cerr << "seat name : " << name << std::endl;
}

void global_registry_handler(void *data, wl_registry *registry, uint32_t id,
                             const char *interface, uint32_t version) {

	std::cout << "Got a registry event for : " << interface << " id " << id
	          << std::endl;
	client_state *state = static_cast<client_state *>(data);

	if (std::string(interface) == std::string(wl_shm_interface.name)) {
		state->shm = static_cast<wl_shm *>(
		    wl_registry_bind(registry, id, &wl_shm_interface, 1));
	} else if (std::string(interface) ==
	           std::string(wl_compositor_interface.name)) {
		state->compositor = static_cast<wl_compositor *>(
		    wl_registry_bind(registry, id, &wl_compositor_interface, 4));
	} else if (std::string(interface) ==
	           std::string(xdg_wm_base_interface.name)) {
		state->wm_base = static_cast<xdg_wm_base *>(
		    wl_registry_bind(registry, id, &xdg_wm_base_interface, 1));
		xdg_wm_base_add_listener(state->wm_base, &wm_base_listener, state);
	} else if (std::string(interface) ==
	           std::string(wl_output_interface.name)) {
		state->output = static_cast<wl_output *>(
		    wl_registry_bind(registry, id, &wl_output_interface, 2));
		wl_output_add_listener(state->output, &output_listener, state);
	} else if (std::string(interface) == std::string(wl_seat_interface.name)) {
		state->seat = static_cast<wl_seat *>(
		    wl_registry_bind(registry, id, &wl_seat_interface, 5));
		wl_seat_add_listener(state->seat, &seat_listener, state);
	}
}

void global_registry_remover(void *data, struct wl_registry *registry,
                             uint32_t id) {
	std::cout << "Got a registry losing event for " << id << std::endl;
}

int main(int argc, char *argv[]) {
	client_state state = {0};

	state.width = 640;
	state.height = 480;

	std::cout << argc << " " << argv[0] << std::endl;
	if (argc == 2) {
		std::string wayland_display = "wayland-" + std::string(argv[1]);
		state.display = wl_display_connect(wayland_display.c_str());
	} else {
		state.display = wl_display_connect(nullptr);
	}
	state.registry = wl_display_get_registry(state.display);
	state.xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);

	wl_registry_add_listener(state.registry, &registry_listener, &state);
	wl_display_roundtrip(state.display);

	state.surface_wl = wl_compositor_create_surface(state.compositor);
	state.surface_xdg =
	    xdg_wm_base_get_xdg_surface(state.wm_base, state.surface_wl);

	xdg_surface_add_listener(state.surface_xdg, &xdg_surface_listener, &state);

	state.toplevel = xdg_surface_get_toplevel(state.surface_xdg);

	wl_surface_commit(state.surface_wl);

	xdg_toplevel_add_listener(state.toplevel, &toplevel_listener, &state);
	xdg_toplevel_set_title(state.toplevel, "Client example");

	wl_callback *cb = wl_surface_frame(state.surface_wl);
	wl_callback_add_listener(cb, &surface_frame_listener, &state);

	while (wl_display_dispatch(state.display)) {
	}
	return 0;
}
