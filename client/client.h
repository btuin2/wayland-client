/*
wayland-client
Copyright (C) 2022 BTuin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CLIENT_H_
#define CLIENT_H_

#include "pointer.h"
#include "shm_file.h"
#include "xdg-shell-client-protocol.h"
#include <cassert>
#include <cstring>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sys/mman.h>
#include <wayland-client.h>

void buffer_release(void *data, wl_buffer *buffer);

wl_buffer *draw_frame(client_state *state);

void xdg_surface_configure(void *data, xdg_surface *surface, uint32_t serial);

void wm_base_ping(void *data, xdg_wm_base *wm_base, uint32_t serial);

void surface_frame_done(void *data, wl_callback *cb, uint32_t time);

void do_nothing(void *data, wl_output *output, uint32_t id);

void output_scale(void *data, wl_output *output, int32_t factor);

void display_handle_geometry(void *data, wl_output *output, int x, int y,
                             int physical_width, int physical_height,
                             int subpixel, const char *make, const char *model,
                             int transform);

void display_handle_mode(void *data, wl_output *output, uint32_t flags,
                         int width, int height, int refresh);

void display_handle_done(void *data, wl_output *output);

void seat_capabilies(void *data, wl_seat *seat, uint32_t capabilities);

void seat_name(void *data, wl_seat *seat, const char *name);

void global_registry_handler(void *data, wl_registry *registry, uint32_t id,
                             const char *interface, uint32_t version);

void global_registry_remover(void *data, struct wl_registry *registry,
                             uint32_t id);

#endif // CLIENT_H_
